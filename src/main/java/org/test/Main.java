package org.test;

import org.testng.TestNG;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<>();
        suites.add("suite/suite_1.xml");
        testNG.setTestSuites(suites);
        testNG.run();
    }
}