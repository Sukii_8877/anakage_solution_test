package org.test.screens;

import com.opencsv.CSVReader;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.test.providers.Provider;
import org.test.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class SolutionsTest {
    @Test()
    public static void newlyAddedSolutionsTableTest() {
        WebDriver driver = Provider.driver;

        //Waiting for the NEWLY_ADDED_SOLUTIONS section to be displayed [ just waiting for the NEWLY ADDED SOLUTIONS text to be displayed ]
        Assert.assertTrue(Utils.idWait(driver, "table-caption-id12"));

        //Scrolling to NEWLY_ADDED_SOLUTIONS
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});",
                driver.findElement(By.xpath("//*[@id=\"solutionDiv\"]/div[1]/div[2]")));

        //Fetching table values
        WebElement tableElement = driver.findElement(By.xpath("//*[@id=\"sample_12\"]"));
        List<List<String>> table = new ArrayList<>();
        for (WebElement row : tableElement.findElements(By.tagName("tr"))) {
            List<String> col = new ArrayList<>();
            for (WebElement cell : row.findElements(By.tagName("td"))) {
                col.add(cell.getText());
            }
            table.add(col);
        }
        Provider.table = table;
    }

    @Test(priority = 1)
    public static void csvRead() {
        try {
            //Reading the csv file present ins assets/csv/test.csv
            FileReader fileReader = new FileReader("assets/csv/test.csv");
            CSVReader csvReader = new CSVReader(fileReader);
            Provider.csvFile = csvReader.readAll();
            if (Provider.csvFile.isEmpty()) {
                Assert.fail("Csv file is Empty");
            }
        } catch (Exception e) {
            Assert.fail("Couldn't find the CSV file in assets");
        }
    }

    @Test(priority = 2, dependsOnMethods = {"csvRead"})
    public static void countTest() {
        List<String[]> csvFile = Provider.csvFile;
        List<List<String>> table = Provider.table;
        //testing the count of the lines in csv file and in the newly added solutions
        if (table.size() == csvFile.size()) {
            System.out.println("Total number of solutions in Csv file ( csv solutions -> " + (csvFile.size() - 1) + " ) and in Newly Added Solutions ( newly added solutions -> " + (table.size() - 1) + " ) are same");
        } else {
            Assert.fail("Total number of solutions in Csv file ( csv solutions -> " + (csvFile.size() - 1) + " ) and in Newly Added Solutions ( newly added solutions -> " + (table.size() - 1) + " ) are different");
        }
    }

    @Test(priority = 3, dependsOnMethods = {"countTest"})
    public static void solutionNumberTest() {
        List<String[]> csvFile = Provider.csvFile;
        List<List<String>> table = Provider.table;

        List<Integer> matched = new ArrayList<>();
        List<Integer> unmatched = new ArrayList<>();

        for (int i = 1; i < csvFile.size(); i++) {
            boolean isMatched = false;
            int csvNumber = (int) (Float.parseFloat(csvFile.get(i)[5].trim().isEmpty() ? "0" : csvFile.get(i)[5]));
            for (int j = 1; j < table.size(); j++) {
                int tableNumber = (int) Float.parseFloat(table.get(j).get(5).trim().isEmpty() ? "0" : table.get(j).get(5));
                if (tableNumber == csvNumber) {
                    matched.add(csvNumber);
                    isMatched = true;
                    break;
                }
            }
            if (!isMatched) {
                unmatched.add(csvNumber);

            }

        }

        System.out.println("Matched Numbers: " + matched);
        System.out.println("Unmatched Numbers: " + unmatched);
        Provider.driver.findElement(By.tagName("body")).sendKeys(Keys.CONTROL + "t");
        Provider.driver.switchTo().window(Provider.driver.getWindowHandle()).get("file:///D:/Projects/Anakage_Solution_Test/test-output/emailable-report.html");
//        ExtentHtmlReporter htmlReporter =  new ExtentHtmlReporter(System.getProperty("user.dir")+"/Reports/extentReport.html");
    }
}
