package org.test.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.test.providers.Provider;
import org.test.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DashBoardScreen {
    @Test
    public static void dashBoardScreenTest() {
        WebDriver driver = Provider.driver;
        //Waiting for this dashboard button to be displayed
        Assert.assertTrue(Utils.xpathWait(driver, "//*[@id=\"pageHeaderHeight\"]/div[2]/div/div[1]/ul/li[2]"));

        //dashboard button
        driver.findElement(By.xpath("//*[@id=\"pageHeaderHeight\"]/div[2]/div/div[1]/ul/li[2]")).click();

    }

    @Test(priority = 1)
    public static void contentManagementButtonTest() {
        WebDriver driver = Provider.driver;

        //Waiting for the content management button to be displayed
        Assert.assertTrue(Utils.idWait(driver, "version"));

        //Content Management button
        driver.findElement(By.id("version")).click();
    }

    @Test(priority = 2)
    public static void solutionButtonTest() {
        WebDriver driver = Provider.driver;

        //Waiting for the solution button to be displayed
        Assert.assertTrue(Utils.idWait(driver, "solutions"));

        //solution button
        driver.findElement(By.id("solutions")).click();
    }



}
