package org.test.screens;

import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.test.global.Global;
import org.test.providers.Provider;
import org.test.utils.Utils;
import org.testng.annotations.Test;

public class LoginScreen {
    @Test()
    static void loginTest() {
        WebDriver driver = Provider.driver;

        //UserName TextBox Element
        WebElement userNameElement = driver.findElement(By.id("userName"));
        userNameElement.sendKeys(Global.USERNAME);

        //Password TextBox Element
        WebElement passwordElement = driver.findElement(By.name("j_password"));
        passwordElement.sendKeys(Global.PASSWORD + Keys.ENTER);

        //Checking if the homepage is Displayed
        try {
            Utils.idWait(driver, "home");
        } catch (Exception e) {
            Assert.fail("Couldn't Login the User");
        }

    }

}
