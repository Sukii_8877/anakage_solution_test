package org.test.providers;

import org.openqa.selenium.WebDriver;

import java.util.List;

public class Provider {
    public static WebDriver driver;
    public static List<List<String>> table;
    public static List<String[]> csvFile;
    public static WebDriver getDriver() {
        return driver;
    }
    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }
}
